<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'devdesig_axis' );

/** MySQL database username */
define( 'DB_USER', 'devdesig_dev' );

/** MySQL database password */
define( 'DB_PASSWORD', 'z9Fbhz0r]r_e' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '6(rW|RAgUCrD)AlN<jR;5-P(~Nv D|D@[z7>su{f4VKuGf;I:s2O^++<ww:zV_6~');
define('SECURE_AUTH_KEY',  'c~YsRWpJz2$+^~YP7!6}I2*``<Mmc!xR;/&ECId=;^wa?QqWWts8912@Y~Hx[t]O');
define('LOGGED_IN_KEY',    '}tZ<W(^$M!t46Ihb ncNy,#*Z3}HyIJhzZsz#T6ll(+IIF|!BfvOg2ZoAKvR||T5');
define('NONCE_KEY',        'RSyS&2*UPUPwp2L$)#5Vlud#4;Pk_p|1]/!)kGndz!2+W+g9?/v]S.5Zu/#]m&rE');
define('AUTH_SALT',        'ZS/p]t#]e(m1z9!D,*V^W&E-}Rk{Z-B{j c*X_+c$N5FjSesA5a>Ys[wOwB+)e+2');
define('SECURE_AUTH_SALT', '@SeLl(M0RD6w3GO>q !s~JZ4F&Hk4|eJwu12lsu5G-?o]~8p5tyDBiTx8W!<Xd,+');
define('LOGGED_IN_SALT',   'YlKX)+UpEs5$]HFZ)1z2gXqHi38]z;PK3f_P>e{oo$?1o#o,Vbc?~=D|^B;PY`U^');
define('NONCE_SALT',       '!_?gwb>hE33Tv`ssP:DS#ao-3mS@0<(@8il:GqdT_PCY*oH;a<+Av~WNMS2eCA%I');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

define('WP_CACHE', true); // Added by WP Hummingbird
/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
